import testcase as tc

class WasRun(tc.TestCase):
    def __init__(self, name):
        self.name = name

    def setUp(self):
        self.log = "setUp "

    def tearDown(self):
        self.log = self.log + "tearDown "

    def testMethod(self):
        self.log = self.log + "testMethod " 

    def testBrokenMethod(self):
        raise Exception
