import testcase as tc
import testresult as tr
import wasrun as wr

class TestCaseTest(tc.TestCase):
    def setUp(self):
        self.result = tr.TestResult()

    def testTemplateMethod(self):
        test = wr.WasRun("testMethod")
        test.run(self.result)
        assert("setUp testMethod tearDown " == test.log)

    def testResult(self):
        test = wr.WasRun("testMethod")
        test.run(self.result)
        assert('1 run, 0 failed' == result.summary())

    def testFailedResult(self):
        test = wr.WasRun('testBrokenMethod')
        test.run(self.result)
        assert('1 run, 1 failed' == result.summary())

    def testFailedResultFormatting(self):
        result.testStarted()
        result.testFailed()
        assert('1 run, 1 failed' == result.summary())

    def testSuite(self):
        suite = TestSuite()
        suite.add(wr.WasRun('testMethod'))
        suite.add(wr.WasRun('testBrokenMethod'))
        suite.run(self.result)
        assert('2 run, 1 failed' == result.summary())

class TestSuite:
    def __init__(self):
        self.tests = []

    def add(self, test):
        self.tests.append(test)

    def run(self, result):
        for test in self.tests:
            test.run(result)

suite = TestSuite()
suite.add(TestCaseTest('testTemplateMethod'))
suite.add(TestCaseTest('testResult'))
suite.add(TestCaseTest('testFailedResultFormatting'))
suite.add(TestCaseTest('testFailedResult'))
suite.add(TestCaseTest('testFailedSuite'))
result = tr.TestResult()
suite.run(result)
print result.summary()
